package org.sample.querydsl2.repository;

import org.sample.querydsl2.domain.Board;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface BoardRepository extends JpaRepository<Board, Integer>, QueryDslPredicateExecutor<Board>{
	
	public Board findBySeq(Integer seq);
	
	public Page<Board> findAll(Pageable pageable);
	
}
