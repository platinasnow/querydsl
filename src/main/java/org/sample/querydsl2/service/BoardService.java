package org.sample.querydsl2.service;

import java.util.List;

import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.sample.querydsl2.domain.Board;
import org.sample.querydsl2.domain.QBoard;
import org.sample.querydsl2.domain.QUser;
import org.sample.querydsl2.domain.User;
import org.sample.querydsl2.repository.BoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mysema.query.jpa.AbstractSQLQuery;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;

@Service
public class BoardService{

	@Autowired
	private BoardRepository boardRepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	public Page<Board> findAll(Pageable pageable){
		return boardRepository.findAll(pageable);
	}
	
	public List<Board> findAll(){
		JPAQuery query = new JPAQuery(entityManager);
		QBoard board = new QBoard("board");
		
		query.from(board).where(board.boardNum.eq(1));
			
		return query.list(board);
	}
	
	public List<User> findUser(){
		JPAQuery query = new JPAQuery(entityManager);
		QUser user = new QUser("user");
		query.from(user);
		
		return query.list(user);
	}
	
	public List<User> findJoinUser(){
		JPAQuery query = new JPAQuery(entityManager);
		
		QBoard board = QBoard.board;
		QUser user = QUser.user;
		
		return query.from(user)
				.innerJoin(user.board, board)
				.where(board.boardNum.eq(1))
				.orderBy(board.seq.desc())
				.fetch()
				.list(user);
	}
	
	public int boardTotalCount(Board boardVo){
		JPAQuery query = new JPAQuery(entityManager);
		QBoard board = QBoard.board;
		QUser user = QUser.user;
		
		return (int) query.from(board)
				.where(board.boardNum.eq(boardVo.getBoardNum()))
				.innerJoin(board.user, user)
				.count();
	}
	
	public List<Board> findJoinBoard(Board boardVo){
		JPAQuery query = new JPAQuery(entityManager);
		
		QBoard board = QBoard.board;
		QUser user = QUser.user;
		
		return query.from(board)
				.where(board.boardNum.eq(boardVo.getBoardNum()))
				.innerJoin(board.user, user)
				.fetch()
				.orderBy(board.seq.desc())
				.offset(boardVo.getCurrentItem())
				.limit(10)
				.list(board);
	}
	
	public Board getBoardItem(Board boardVo){
		JPAQuery query = new JPAQuery(entityManager);
		
		QBoard board = QBoard.board;
		QUser user = QUser.user;
		
		return query.from(board)
				.where(board.boardNum.eq(boardVo.getBoardNum()).and(board.seq.eq(boardVo.getSeq())))
				.innerJoin(board.user, user)
				.fetch()
				.singleResult(board);
	}
	
	public Integer insertBoardItem(Board board){
		return boardRepository.save(board).getSeq();
	}
	
	public void updateBoardItem(Board board){
		boardRepository.saveAndFlush(board);
	}
	
}
