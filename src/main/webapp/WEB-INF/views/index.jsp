<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.1.0.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC2jAHD_ucqEsbTjtje-Gymw-AdffEvXoc&sensor=false">
</script>

<script>
function initialize()
{
var mapProp = {
  center:new google.maps.LatLng(51.508742,-0.120850),
  zoom:5,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };
var map=new google.maps.Map(document.getElementById("googleMap")
  ,mapProp);
}

google.maps.event.addDomListener(window, 'load', initialize);

function goMap(lat,lng){
	var mapProp = {
			  center:new google.maps.LatLng(lat,lng),
			  zoom:17,
			  mapTypeId:google.maps.MapTypeId.ROADMAP
			  };
			var map=new google.maps.Map(document.getElementById("googleMap")
			  ,mapProp);
}
</script>
</head>

<body>
<div id="googleMap" style="width:500px;height:380px;"></div>

</body>
<script type="text/javascript">
/*
$.post('http://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&sensor=true_or_false','', function(result){
	console.log(result);
});
*/
function search(address){
	$.post('http://maps.googleapis.com/maps/api/geocode/json?address='
			+ encodeURIComponent(address) 
			+'&sensor=true','', function(result){
		console.log(result);
		var lat = result.results[0].geometry.location.lat;
		var lng = result.results[0].geometry.location.lng;
		goMap(lat,lng);
		
	});	
}
</script>

</html>