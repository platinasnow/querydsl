<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  

<%@include file="include.jsp" %>
<style type="text/css">
	thead>tr>th{text-align: center;}
	tbody>tr>td:nth-child(1){width:80px; text-align: center;}
	tbody>tr>td:nth-child(3){width:110px; text-align: center;}
	tbody>tr>td:nth-child(4){width:130px; text-align: center;}
	tbody>tr>td:nth-child(5){width:70px; text-align: center;}
	tbody>tr:HOVER{color:#da8c92;cursor: pointer;}
	.menu-wrap{text-align: right;}
	.form-wrap{text-align: center;}
	.table-empty{width:100%;text-align: center;padding: 20px 0;font-weight: bold;}
</style>

<div class="container">
	<h2 class="sub-header">Section title</h2>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>제목</th>
					<th>작성자</th>
					<th>작성일</th>
					<th>조회수</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach items="${list }" var="list">
				<tr onclick="onView('${list.seq }');"> 
					<td>${list.seq }</td>
					<td>${list.title }</td>
					<td>${list.user.name }</td>
					<td> <fmt:formatDate value="${list.recodeDate }" pattern="YYYY-MM-dd"   /></td>
					<td>${list.hit }</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
		<c:if test="${empty list}">
			<div class="table-empty">게시글이 존재하지 않습니다.</div>
		</c:if>
	</div>	
	<div class="menu-wrap">
		<button type="button" onclick="onWrite()" class="btn btn-primary">쓰기</button>
		<button type="button" onclick="onList()" class="btn btn-primary">목록</button>
	</div>
	<form class="form-wrap" method="post">
		<ul class="pagination">
			<li><a href="javascript:goPage('${board.jumpPrevPage}')">&laquo;</a></li>
			<c:forEach begin="${board.pageBegin }" end="${board.pageEnd }" var="idx">
				<c:if test="${board.currentPage == idx}"><li class="active"><a href="#">${idx }</a></li></c:if> 
				<c:if test="${board.currentPage != idx}"><li><a href="javascript:goPage('${idx}')">${idx }</a></li></c:if>
			</c:forEach>
			<li><a href="javascript:goPage('${board.jumpNextPage }')">&raquo;</a></li>
		</ul>
		<%-- 
		<div class="search">
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
					<select name="searchOption" class="searchOption btn btn-default ">
						<option value="title">제목</option>
						<option value="contents">내용</option>
						<option value="name">작성자</option>
					</select>
					</span>
					<input type="hidden" class="searchOptionVal" value="${board.searchOption }" />
					<input type="text" name="searchInput" class="form-control" value="${board.searchInput }" />
					 <span class="input-group-btn">
						<button type="button" onclick="onSearch()" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 검색</button>
					</span>
				</div>
			</div>	
		</div>
		 --%>
		<input type="hidden" name="boardNum" id="boardNum" value="${board.boardNum }" />
		<input type="hidden" name="seq" id="seq" value="0" />
		<input type="hidden" name="page" class="page" value="${board.currentPage }" />
	</form>
</div>

<script type="text/javascript">

$('.searchOption').val($('.searchOptionVal').val());
var boardForm = document.forms[0];
var onWrite = function(){
	boardForm.action = '/board/write';
	boardForm.submit(); 
};
var onList = function(){
	location.href = '/board/list/'+$('#boardNum').val();
};
var goPage = function(page){
	$('.page').val(page);
	boardForm.submit();
};

var onSearch = function(){
	$('.page').val(1);
	boardForm.submit();
};

var onView = function(seq){
	$("#seq").val(seq);
	boardForm.action = '/board/view/'+seq;
	boardForm.submit();
};
</script>
