<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@include file="include.jsp" %>
<style type="text/css">
#boardWriteForm{margin: 30px 0;}
.buttonDiv{text-align: center;margin: 10px 0;}
.file-wrap{overflow: hidden;}
 .file-wrap-title{float: left; margin: 10px 5px;}
 .file-wrap-contents{float: left; margin: 0 5px;}
 .file-item-wrap{border: 1px solid #ccc; margin: -1px 0 0 0; padding: 5px 20px; overflow: hidden;}
 .file-item-title{float: left; width:740px; }
 .file-delete-btn{float: left;font-weight: bold;cursor: pointer;}
 .file-delete-btn:HOVER {color:#d9230f;}
 .upload-btn{margin: 5px 0;}
</style>

<div class="container">
	<form id="boardWriteForm" method="post">
		<div>
			<div><b>제목</b></div>
			<input type="text" id="title" name="title" value="${item.title }" maxlength="100" style="width: 100%;"/>
		</div>
		<div class="contentDiv">
		 	<textarea id="contents" name="contents" rows="30" style="width:100%;">${item.contents}</textarea>
		</div>
		<div class="buttonDiv">
			<c:if test="${modify == 'true'}">
				<button type="button" class="btn btn-primary" onclick="SaveContents_Click('modifySubmit')">수정</button>
			</c:if>
			<c:if test="${modify != 'true'}">
				<button type="button" class="btn btn-primary" onclick="SaveContents_Click('writeSubmit')">쓰기</button>
			</c:if>
			 <button type="button" class="btn btn-primary" onclick="history.go(-1);"> 취소</button>
			 <input type="hidden" name="seq" value="${board.seq}"/> 
			 <input type="hidden" name="boardNum" value="${board.boardNum }" />
			 <input type="hidden" class="fileSeq" name="fileSeqs" value="${fileSeq}" />
		</div>
	</form>
</div>
<script type="text/javascript">
function SaveContents_Click(action) {
    if (document.getElementById("title").value == "") {
        alert('제목을 입력하여야합니다.');
        return;
    } else if(jQuery("#title").val().length > 100 ){
		alert("제목은 100자를 넘을 수 없습니다.");
		return;
	} 
    writeSubmit(action);
}	
	
var writeSubmit = function(action){
	var boardWriteForm = document.getElementById("boardWriteForm");  
	boardWriteForm.action = action;              
	boardWriteForm.submit();  
};
</script>


