package org.sample.querydsl2.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QBoard is a Querydsl query type for Board
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QBoard extends EntityPathBase<Board> {

    private static final long serialVersionUID = 1889018823L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QBoard board = new QBoard("board");

    public final org.sample.querydsl2.util.QPagination _super = new org.sample.querydsl2.util.QPagination(this);

    public final NumberPath<Integer> author = createNumber("author", Integer.class);

    public final NumberPath<Integer> boardNum = createNumber("boardNum", Integer.class);

    public final StringPath contents = createString("contents");

    //inherited
    public final NumberPath<Integer> currentItem = _super.currentItem;

    //inherited
    public final NumberPath<Integer> currentPage = _super.currentPage;

    public final NumberPath<Integer> hit = createNumber("hit", Integer.class);

    //inherited
    public final NumberPath<Integer> itemPerPage = _super.itemPerPage;

    //inherited
    public final NumberPath<Integer> jumpNextPage = _super.jumpNextPage;

    //inherited
    public final NumberPath<Integer> jumpPrevPage = _super.jumpPrevPage;

    //inherited
    public final NumberPath<Integer> nextPage = _super.nextPage;

    //inherited
    public final NumberPath<Integer> page = _super.page;

    //inherited
    public final NumberPath<Integer> pageBegin = _super.pageBegin;

    //inherited
    public final NumberPath<Integer> pageCount = _super.pageCount;

    //inherited
    public final NumberPath<Integer> pageEnd = _super.pageEnd;

    public final DateTimePath<java.util.Date> recodeDate = createDateTime("recodeDate", java.util.Date.class);

    public final NumberPath<Integer> seq = createNumber("seq", Integer.class);

    public final StringPath title = createString("title");

    //inherited
    public final NumberPath<Integer> totalItemCount = _super.totalItemCount;

    public final QUser user;

    public QBoard(String variable) {
        this(Board.class, forVariable(variable), INITS);
    }

    public QBoard(Path<? extends Board> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QBoard(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QBoard(PathMetadata<?> metadata, PathInits inits) {
        this(Board.class, metadata, inits);
    }

    public QBoard(Class<? extends Board> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.user = inits.isInitialized("user") ? new QUser(forProperty("user")) : null;
    }

}

